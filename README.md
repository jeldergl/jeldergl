I joined GitLab in May of 2019 to focus on the visual design of the product. I care about the relationship between product functionality and the consistency of the UI. I focus on creating seamless, performant, and accessible experiences for users as they engage in everything GitLab has to offer. I believe that [a great UI is invisible](https://tympanus.net/codrops/2013/03/21/a-great-ui-is-invisible/) — transparency is one of our values after all.

* I currently live in central Minnesota, USA, with my wife and our three youngsters.
* My normal working hours are around 7am – 4pm in the [CT Timezone](https://time.is/Minneapolis).
* I’ve been designing professionally for 20+ years and work on everything from UI design to logos and packaging. I also do some development.
* I care a great deal about accessibility and tailor my work as such.
* Mountain biking, camping, and boating are some of the things I enjoy most when not working.

How I think about work:

* Distill concepts into responsible and honest work.
* Digital goods can be durable goods too.
* Constraints create flow and movement.
* Think in systems, design in moments.
* Follow your users, not trends.
* Clarity over embellishement.
* Iteration is perfection.
* Learn with grace.

As part of the Foundations team I specifically focus on:

* Product UI design.
* Maintaining the [Pajamas UI Kit](https://www.figma.com/community/file/781156790581391771/) in Figma.
* [Pajamas](https://design.gitlab.com/) documentation.
* [GitLab UI](https://gitlab-org.gitlab.io/gitlab-ui/) components.
* Product [iconography and illustration](https://gitlab-org.gitlab.io/gitlab-svgs/).
* Accessibility audits and evaluations.
